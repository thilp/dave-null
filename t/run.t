use 5.010_001;
use Test::More;
use IPC::Run qw/ run timeout /;
use Path::Class;
use autodie;

my $PROGRAM_NAME = file('davenull.pl');
my $SAMPLES_DIR  = dir(qw/ t samples /);
my $EMAIL_VALID  = 1;                      # same as EMAIL_VALID in davenull.pl

sub davenull {
    my $pcf = shift;
    my ( $out, $err );

    open my $fh, '<', "$pcf";

    run [ 'perl', "$PROGRAM_NAME", qw/ --debug --brutal / ],
      $fh, \$out, \$err, timeout(3);

    return { status => $? >> 8, out => $out, err => $err };
}

sub dave_accept {
    my $pcf = file( $SAMPLES_DIR, shift() );
    die "Can't read $pcf\n" unless $pcf->stat && -r $pcf->stat;
    my $result = davenull($pcf);
    ok $result->{status} == $EMAIL_VALID, "$pcf accepted";
}

sub dave_reject_like {
    my ( $filename, $regex ) = @_;
    my $pcf = file( $SAMPLES_DIR, $filename );
    die "Can't read $pcf\n" unless $pcf->stat && -r $pcf->stat;
    my $result = davenull($pcf);
    ok $result->{status} != $EMAIL_VALID, "$pcf rejected";
    like $result->{err}, $regex, "$pcf produced the expected error";
}

dave_accept('good_test.mail');
dave_accept('good_logcheck.mail');

# Test for BB#1: "Fail on ugly content-type"
dave_reject_like( 'bad_multipart-relaxed.mail',
    qr{'multipart/related' interdit} );

dave_reject_like( 'bad_signatureTooLong.mail', qr/\bplus de 4 lignes\b/ );

dave_reject_like( 'bad_signatureWithoutWhitespace.mail',
    qr/\bespace manquante\b/ );

done_testing();
